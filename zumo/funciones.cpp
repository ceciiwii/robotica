#include <Arduino.h>
#include <SPI.h>
#include <RF24.h>
#include "funciones.h"



// PARAMETROS
RF24 radio(0, 1);  // Pin CE y CSN
const byte direccion[6] = {0xCC, 0xCE, 0xCC, 0xCE, 0xCC};

// SETUP 
void init_rf()
{
  radio.openReadingPipe(1,direccion);
  radio.startListening();
}

// LOOP
Datos lectura_rf()
{
  Datos datos;

  if (radio.available()) 
  {
    char texto[32] = "";
    // Lee el mensaje
    radio.read(&texto, sizeof(texto));
    
    char S_vertical[16] = "";
    char S_horizontal[16] = "";
    
    // Tokenizar el texto usando la coma como delimitador
    char *token = strtok(texto, ",");
    
    // Copiar el primer token 
    if (token != NULL) {
      strncpy(S_vertical, token, sizeof(datos.S_vertical) - 1);
      S_vertical[sizeof(datos.S_vertical) - 1] = '\0';  // Asegura que la cadena esté terminada
    }

    // Obtener el siguiente token
    token = strtok(NULL, ",");

    // Copiar el segundo token a 
    if (token != NULL) {
      strncpy(S_horizontal, token, sizeof(datos.S_horizontal) - 1);
      S_horizontal[sizeof(datos.S_horizontal) - 1] = '\0';  // Asegura que la cadena esté terminada
    }
  }
  return datos;
}