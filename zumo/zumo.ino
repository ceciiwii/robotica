#include <SPI.h>
#include <RF24.h>
#include <Wire.h>
#include <Zumo32U4.h>
#include "funciones.h"
/* Este codigo corresponde a un seguider de linea PID, que sigue una linea negra en un ambiente de color blanco. 
Este codigo funciona para un zumo con motores 75:1, para otro tipo de robot, se deben realizar modificaciones.

Esta demostración requiere que se instale una placa de sensores frontales de Zumo 32U4.
Estos deben estar configurados en el pin 4 a DN4 y el pin 20 a DN2. */

// Definir un valor menor a 400 limitara la velocidad maxima a la que se movera el robot.
const uint16_t maxSpeed = 200;

Zumo32U4Buzzer buzzer;
Zumo32U4LineSensors lineSensors;
Zumo32U4Motors motors;
Zumo32U4ButtonA buttonA;
Zumo32U4LCD display;

int16_t lastError = 0;

#define NUM_SENSORS 5
unsigned int lineSensorValues[NUM_SENSORS];

// Definicion de caracteres de niveles de barra.
void loadCustomCharacters()
{
  static const char levels[] PROGMEM = {
    0, 0, 0, 0, 0, 0, 0, 63, 63, 63, 63, 63, 63, 63
  };
  display.loadCustomCharacter(levels + 0, 0);  // 1 bar
  display.loadCustomCharacter(levels + 1, 1);  // 2 bars
  display.loadCustomCharacter(levels + 2, 2);  // 3 bars
  display.loadCustomCharacter(levels + 3, 3);  // 4 bars
  display.loadCustomCharacter(levels + 4, 4);  // 5 bars
  display.loadCustomCharacter(levels + 5, 5);  // 6 bars
  display.loadCustomCharacter(levels + 6, 6);  // 7 bars
}

void printBar(uint8_t height)
{
  if (height > 8) { height = 8; }
  const char barChars[] = {' ', 0, 1, 2, 3, 4, 5, 6, (char)255};
  display.print(barChars[height]);
}

void calibrateSensors()
{
  display.clear();

  // Codigo de autocalibracion de los sensores seguidores de linea.
  delay(1000);
  for(uint16_t i = 0; i < 120; i++)
  {
    if (i > 30 && i <= 90)
    {
      motors.setSpeeds(-200, 200);
    }
    else
    {
      motors.setSpeeds(200, -200);
    }

    lineSensors.calibrate();
  }
  motors.setSpeeds(0, 0);
}

// Muestra un grafico de barras de las lecturas del sensor
// Espera la confirmacion del usuario para terminar, apretando el boton A.
void showReadings()
{
  display.clear();

  while(!buttonA.getSingleDebouncedPress())
  {
    lineSensors.readCalibrated(lineSensorValues);

    display.gotoXY(0, 0);
    for (uint8_t i = 0; i < NUM_SENSORS; i++)
    {
      uint8_t barHeight = map(lineSensorValues[i], 0, 1000, 0, 8);
      printBar(barHeight);
    }
  }
}

void setup()
{ 
  // Serial.begin(9600);  // Inicializa la comunicación serial
  init_rf();

  lineSensors.initFiveSensors();
  loadCustomCharacters();

  // Reproduce un sonido de bienvenida


  // Wait for button A to be pressed and released.
  display.clear();
  display.print(F("Pres. A"));
  display.gotoXY(0, 1);
  display.print(F("Para calib."));
  buttonA.waitForButton();

  calibrateSensors();

  showReadings();

  // Reproduce un sonido de confirmacion, luego espera que termine este para comenzar.
  display.clear();
  display.print(F("Inicio!"));
  for (int i = 0; i < 3; i++)
  {
    delay(1000);
    buzzer.playNote(NOTE_G(3), 200, 15);
  }
  delay(1000);
  buzzer.playNote(NOTE_G(4), 500, 15);
  delay(1000);
  while(buzzer.isPlaying());
}

void loop()
{
  motors.setSpeeds(100, 100);
  int valor = lectura_rf();
  // Toma la posicion de la linea.  Debemos proveeer el valor del argumento
  // "lineSensorValues" para leer readLine() aca, incluso
  // si no estamos interesados en las lecturas individuales de los sensores
  int16_t position = lineSensors.readLine(lineSensorValues);

  // el "error" es que tan lejos estamos del centro de la
  // linea, en donde corresponde al valor 2000.
  int16_t error = position - 2000;

  // Obtener la diferencia de velocidad del motor utilizando términos PID proporcionales y derivativos
  // (por lo general, el término integral no es muy útil para seguir líneas). Aquí estamos usando una constante proporcional
  // de 1/4 y una constante derivativa de 6, lo que debería funcionar bastante bien para muchas opciones de motores Zumo.
  // Probablemente querrás usar el método de prueba y error para ajustar estas constantes para tu
  // Zumo en particular y el trazado de la línea.
  int16_t speedDifference = error / 4 + 6 * (error - lastError);

  lastError = error;

  String posicion = "";  // Variable para almacenar la posición del semáforo
  // Velocidades individuales de cada motor.  el signo de speedDifference
  // determina si el motor se movera a la izquierda o derecha.
  int16_t leftSpeed = (int16_t)maxSpeed + speedDifference;
  int16_t rightSpeed = (int16_t)maxSpeed - speedDifference;

  /* Limitamos nuestras velocidades de motor para que estén entre 0 y maxSpeed.
  Un motor siempre girará a maxSpeed y el otro
  girará a maxSpeed - |speedDifference| si es positivo,
  de lo contrario, se quedará inmóvil. Para algunas aplicaciones, 
  es posible que desees permitir que la velocidad del motor sea negativa
  para que pueda girar en reversa. */
  leftSpeed = constrain(leftSpeed, 0, (int16_t)maxSpeed);
  rightSpeed = constrain(rightSpeed, 0, (int16_t)maxSpeed);

  motors.setSpeeds(leftSpeed, rightSpeed);


  if (lineSensorValues[0] > 500 && lineSensorValues[NUM_SENSORS - 1] > 500) 
  { // Sentido Vertical
    posicion = "vertical";
    switch (valor) 
    {
      case 02:
      motors.setSpeeds(leftSpeed, rightSpeed); // Valor máximo es 400.
      break;
      case 12:
      motors.setSpeeds(0, 0); // Se detienen los motores.
      break;
    }
  }

  if (lineSensorValues[5] > 500 && lineSensorValues[NUM_SENSORS - 2] > 500) 
  { // Sentido horizontal
    posicion = "horizontal";
    switch (valor) 
    {
      case 20:
      motors.setSpeeds(leftSpeed, rightSpeed); // Valor máximo es 400.
      break;
      case 21:
      motors.setSpeeds(0, 0); // Se detienen los motores.
      break;
    }
  }

}
