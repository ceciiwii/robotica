#include <SPI.h>
#include <RF24.h>
#include "funciones.h"

void setup() {
  Serial.begin(9600);  // Inicializa la comunicación serial
  init_rf();
  Serial.print("listo");

  // Configurar pines como salidas
  init_pines();
}

void loop() // Verde=0, amarillo=1, rojo=2
{
  SVverde_SHrojo();
  printStatus("01");
  delay(5000);

  SVamarillo_SHrojo();
  printStatus("12");
  delay(2000);

  SVrojo_SHverde();
  printStatus("02");
  delay(5000);

  SHamarillo_SVrojo();
  printStatus("21");
  delay(2000);

}
